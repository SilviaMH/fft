# FFT

Este programa recibe una señal del micrófono y muestra tres gráficas:
- La señal original.
- La gráfica en el dominio de la frecuencia al aplicar FFT.
- La señal recuperada al aplicar IFFT.