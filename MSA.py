import pyaudio
import time
import numpy as np
import threading
import sys
import random
from cmath import exp, pi

def computefft(values):
    """
    Computes the fast Fourier transform (FFT) 
    """
    nval = len(values)

    if nval == 1: return values

    t = np.exp(-2j * np.pi / nval)
    y = [0] * nval
    w = 1+0j

    valuesE = computefft(values[::2])
    valuesD = computefft(values[1::2])

    for k in range(nval // 2):
        y[k] = valuesE[k] + w * valuesD[k]
        y[k + nval // 2] = valuesE[k] - w * valuesD[k]
        w = w * t

    return y

def computeifft(values):
    """
    Computes the inverse fast Fourier transform (IFFT) 
    """
    nval = len(values)

    if nval == 1: return values

    t = exp(2 * pi * 1j / nval)
    y = [0] * nval
    w = 1+0j

    valuesE = computeifft(values[::2])
    valuesD = computeifft(values[1::2])

    for k in range(nval // 2):
        y[k] = (valuesE[k] + w * valuesD[k])
        y[k + nval // 2] = (valuesE[k] - w * valuesD[k])
        w = w * t

    return y

def getFFT(data,rate):
    """Given some data and rate, returns FFTfreq and FFT."""
    afft=computefft(data)
    freq=np.fft.fftfreq(len(afft),1.0/rate)
    return freq, afft

def getIFFT(data):
    """Given some data and rate, returns IFFT."""
    ifftc=computeifft(data)
    n = len(ifftc)
    rifft=[(i/n).real for i in ifftc]
    return rifft

class MSA():
    def __init__(self,device=None,rate=None,updatesPerSecond=10):
        self.p=pyaudio.PyAudio()
        self.chunk=4096 # gets replaced automatically
        self.updatesPerSecond=updatesPerSecond
        self.chunksRead=0
        self.device=device
        self.rate=rate

    def valid_low_rate(self,device):
        """
        Set the rate to the lowest supported audio rate.
        """
        for testrate in [44100]:
            if self.valid_test(device,testrate):
                return testrate
        print("No se ha encontrado la tasa de operación.",device)
        return None

    def valid_test(self,device,rate=44100):
        """
        Given a device ID and a rate, return TRUE/False if it's valid.
        """
        try:
            self.info=self.p.get_device_info_by_index(device)
            if not self.info["maxInputChannels"]>0:
                return False
            stream=self.p.open(format=pyaudio.paInt16,channels=1,
               input_device_index=device,frames_per_buffer=self.chunk,
               rate=int(self.info["defaultSampleRate"]),input=True)
            stream.close()
            return True
        except:
            return False

    def valid_input_devices(self):
        """
        See which devices can be opened for microphone input.
        """
        mics=[]
        for device in range(self.p.get_device_count()):
            if self.valid_test(device):
                mics.append(device)
        if len(mics)==0:
            print("No se encontró un micrófono!")
        else:
            print("Se encontraron %d dispositivos: %s"%(len(mics),mics))
        return mics

    def initiate(self):
        """
        Chooses up a microphone 
        """
        if self.device is None:
            self.device=self.valid_input_devices()[0] #pick the first one
        if self.rate is None:
            self.rate=self.valid_low_rate(self.device)
        self.chunk = int(self.rate/self.updatesPerSecond) # hold one tenth of a second in memory
        if not self.valid_test(self.device,self.rate):
            self.device=self.valid_input_devices()[0] #pick the first one
            self.rate=self.valid_low_rate(self.device)
        self.dataxx=np.arange(self.chunk)/float(self.rate)
        self.datax = self.dataxx[:256]

    def close(self):
        """
        Closes the thread
        """
        self.keepRecording=False #the threads should self-close
        while(self.t.isAlive()): #wait for all threads to close
            time.sleep(.1)
        self.stream.stop_stream()
        self.p.terminate()

    def stream_readchunk(self):
        """Reads audio and then relaunches itself to continue getting audio"""
        try:
            self.dataa = np.fromstring(self.stream.read(self.chunk),dtype=np.int16)
            self.data = self.dataa[:256]
            self.fftx, cfft = getFFT(self.data, self.rate)
            self.fft=np.abs(cfft)
            self.ifft = getIFFT(cfft)      

        except Exception as E:
            print(E,"\n"*5)
            self.keepRecording=False
        if self.keepRecording:
            self.stream_thread_new()
        else:
            self.stream.close()
            self.p.terminate()
            print("Flujo detenido.")
        self.chunksRead+=1

    def stream_thread_new(self):
        """
        Creates a new thread which target is the stream reading method
        """
        self.t=threading.Thread(target=self.stream_readchunk)
        self.t.start()

    def stream_start(self):
        """
        Adds stream data to self.data 
        """
        self.initiate()
        self.keepRecording=True 
        self.data=None 
        self.fft=None
        self.ifft=None
        self.dataFiltered=None 
        self.stream=self.p.open(format=pyaudio.paInt16,channels=1,
                      rate=self.rate,input=True,frames_per_buffer=self.chunk)
        self.stream_thread_new()

if __name__=="__main__":
    testarray = [4,3,1,9,5,6,3,2]
    print(testarray)
    ffttest = computefft(testarray)
    iffttest2 = getIFFT(ffttest)
    print(iffttest2)