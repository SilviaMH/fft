from PyQt5 import QtGui,QtCore

import sys
import ui_main
import numpy as np
import pyqtgraph
import MSA

class ExampleApp(QtGui.QMainWindow, ui_main.Ui_MainWindow):
    def __init__(self, parent=None):
        pyqtgraph.setConfigOption('background', 'k') 
        super(ExampleApp, self).__init__(parent)
        self.setupUi(self)
        self.grFFT.plotItem.showGrid(True, True, 0.7)
        self.grFFT.plotItem.setLabel('bottom',text='Frecuencia (Hz)')
        self.grPCM.plotItem.showGrid(True, True, 0.7)
        self.grPCM.plotItem.setLabel('bottom',text='Tiempo (s)')
        self.grOriginal.plotItem.showGrid(True, True, 0.7)
        self.grOriginal.plotItem.setLabel('bottom',text='Tiempo (s)')
        self.maxFFT=0
        self.maxPCM=0
        self.maxOriginal = 0
        self.sa = MSA.MSA(rate=44100,updatesPerSecond=20)
        self.sa.stream_start()

    def update(self):
        if not self.sa.data is None and not self.sa.fft is None:
            pcmMax=np.max(np.abs(self.sa.data))
            originalMax=np.max(np.abs(self.sa.ifft))
            if pcmMax!=self.maxPCM:
                self.maxPCM=pcmMax
                #self.grPCM.plotItem.setRange(yRange=[-pcmMax,pcmMax])
                self.grPCM.plotItem.setRange(yRange=[-50,50])
            if np.max(self.sa.fft)!=self.maxFFT:
                self.maxFFT=np.max(np.abs(self.sa.fft))
                ##self.grFFT.plotItem.setRange(yRange=[0,self.maxFFT])
                self.grFFT.plotItem.setRange(yRange=[0,1])
            if originalMax!=self.maxOriginal: #checar escala
                self.maxOriginal=originalMax
                #self.grOriginal.plotItem.setRange(yRange=[-originalMax,originalMax])
                self.grOriginal.plotItem.setRange(yRange=[-50,50])
            pen=pyqtgraph.mkPen(color='m')
            self.grPCM.plot(self.sa.datax,self.sa.data,pen=pen,clear=True)
            pen=pyqtgraph.mkPen(color='m')
            self.grOriginal.plot(self.sa.datax,self.sa.ifft,pen=pen,clear=True)
            pen=pyqtgraph.mkPen(color='c')
            self.grFFT.plot(self.sa.fftx[:int(len(self.sa.fftx)/2)],self.sa.fft[:int(len(self.sa.fft)/2)]/self.maxFFT,pen=pen,clear=True)
        QtCore.QTimer.singleShot(1, self.update) 

if __name__=="__main__":
    app = QtGui.QApplication(sys.argv)
    form = ExampleApp()
    form.show()
    form.update() 
    app.exec_()